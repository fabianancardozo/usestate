import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Counter } from './components/Counter';
import { User } from './components/User';

function App() {
  return (
    <div className="App">
      <header className="App-header">
      <h1>useState en React con TypeScript</h1>
      <hr/>
      <Counter/>
      <User/>
      </header>
    </div>
  );
}

export default App;
