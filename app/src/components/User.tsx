import { useState } from "react";

interface User {
    uid: string;
    name: string;
}

export const User = () => {

    const [user, setUser] = useState<User>();

    const login = () => {
        setUser({
            uid: 'ABC123',
            name: 'Fernando'
        });
    }


    return (
        <div className="mt-5">
            <h3>Nombre de Usuario</h3>
            <button
            onClick={login}
                className="btn btn-outline-primary"
            >
                Login
            </button>
            <pre>{JSON.stringify(user)} </pre>
        </div>
    );
};
