//rafc
import { useState } from "react";

export const Counter = () => {

    //counter (state) es el valor de estado actual
    //setCounter (setState) es la funcion que actualiza el estado
    //estado actual, actualizar estado.
    const [counter, setCounter] = useState(0);

    //:void significa que la funcion no retorna nada   
    //(number) es lo que queremos recibir 
    const incrementar = (num: number = 1) :void => {
        setCounter (counter + num);


    //onClick por defecto envia un event, no un number por ejemplo.
    //para que esto ocurra debemos llamar a la funcion increment con una funcion '() =>'
    //en el primer button el parametro esta vacio por que ya dijimos que incremente 1
    //en el segundo buton el parametro es 2, para que incremente en 2
}
    return(
        <div className="container mt-5">

            <h3>Counter Component</h3>
            <span>Valor: {counter}</span>
            <br/>
            <button 
                onClick={ () => incrementar()}
                className="btn btn-outline-primary mt-2">
                +1
            </button>
            <p/>
            <button 
                onClick={() => incrementar(2)}
                className="btn btn-outline-primary mt-2">
                +2
            </button>
            <p/>
            <button 
                onClick={() => setCounter(0)}
                className="btn btn-outline-primary mt-2">
                Reset
            </button>

        </div>
    )
}